<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('teams')->insert(
            [
                [
                    "name" => "Liverpool",
                    "image" => "https://resources.premierleague.com/premierleague/badges/25/t14.png",
                    "strength" => 30
                ],
                [
                    "name" => "Manchester City",
                    "image" => "https://resources.premierleague.com/premierleague/badges/25/t43.png",
                    "strength" => 30
                ],
                [
                    "name" => "Arsenal",
                    "image" => "https://resources.premierleague.com/premierleague/badges/25/t3.png",
                    "strength" => 25
                ],
                [
                    "name" => "Chelsea",
                    "image" => "https://resources.premierleague.com/premierleague/badges/25/t8.png",
                    "strength" => 15
                ]
            ]
        );
    }
}
