<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeamsController;
use App\Http\Controllers\MatchsController;
use App\Http\Controllers\ResultsController;
use App\Http\Controllers\PredictionsController;

Route::get('/', function () {
    return view('home');
});

Route::get('/simulation', function () {
    return view('simulation');
});

Route::get('/teams', [TeamsController::class, 'get'])->name('teams');
Route::get('/matchs', [MatchsController::class, 'get'])->name('matchs');
Route::post('/results', [ResultsController::class, 'get'])->name('results');
Route::post('/predictions', [PredictionsController::class, 'get'])->name('predictions');
