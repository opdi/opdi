@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Teams</div>

                <div class="card-body">
                    <table id="teams" class="table table-striped">
                        <tbody>

                        </tbody>
                    </table>
                </div>

                <div class="card-footer">
                    <button onclick="generateFixtures()" class="btn btn-info" id="generate_fixtures_btn">Generate Fixtures</button>
                </div>
            </div>

        </div>
    </div>
    <br/>
    <div class="justify-content-center" id="container_fixtures">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Generated Fixtures
                </div>
                <br/>
                <div class="row justify-content-center" id="fixtures">

                </div>
                <div class="card-footer">
                    <a href="/simulation" class="btn btn-info">Start Simulation</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
