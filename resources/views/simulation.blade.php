@extends('layouts.app')

@section('content')
<div class="container">
    <div id="simulation">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">League</div>
                    <div class="card-body">
                        <table id="league" class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Teams</th>
                                <th scope="col">PTS</th>
                                <th scope="col">P</th>
                                <th scope="col">W</th>
                                <th scope="col">D</th>
                                <th scope="col">L</th>
                                <th scope="col">GD</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button onclick="playAllWeeks()" class="btn btn-info">Play All Weeks</button>
                        <button onclick="window.location.reload()" class="btn btn-danger disabled">Reset Simulation</button>
                    </div>
                </div>

            </div>
        </div>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <button onclick="getNextWeekResults()" class="btn btn-info" style="float: left">
                            Play Next Week</button>
                    </div>

                    <div class="card-body text-center" id="week_results_holder">

                    </div>
                </div>

            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">Championship Predictions</div>

                    <div class="card-body">
                        <table id="predictions" class="table table-striped">
                            <tbody>
                            <tr>
                                <th>* predictions will be shown after the 4th week</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
