<?php

namespace App\Models;

class Results
{
    protected $matchs;

    protected $teams;

    private $max_goal = 5;

    public function __construct(Teams $teams, Matchs $matchs)
    {
        $this->teams = $teams;
        $this->matchs = $matchs;
    }

    public function getByMatches(string $teamOne, string $teamTwo) : array
    {
        $winner = $this->teams->compare($teamOne, $teamTwo);
        $looser = $teamOne === $winner ? $teamTwo : $teamOne;

        $resultWinner = rand(1, $this->max_goal);
        $resultLooser = ($resultWinner - 2) < 0 ? 0 : $resultWinner - rand(0, 2);

        return [
            $winner => $resultWinner,
            $looser => $resultLooser
        ];
    }

    public function getByWeeks(array $week) : array
    {
        $results = [];
        foreach ($week as $key => $match) {
            $results[$key] = $this->getByMatches($match[0], $match[1]);
        }
        return $results;
    }
}
