<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

class Teams extends Model
{
    protected $table = 'teams';

    protected $fillable = [
        'name',
        'image',
        'strength'
    ];

    public $team_list;

    public function get() : object
    {
        if (empty($this->team_list))
            $this->team_list = Teams::all();

        return $this->team_list;
    }

    public function getNames() : array
    {
        $team_list = $this->get();

        $teamArr = [];
        foreach ($team_list as $row) {
            $teamArr[] = $row["name"];
        }

        if (empty($teamArr) || count($teamArr) < 2)
            return 'not enough teams to create league';

        return $teamArr;
    }

    public function getStrength(String $name) : string
    {
        $team_list = $this->get();

        $teamArr = [];
        foreach ($team_list as $row) {
            $teamArr[] = $row;

            if(trim($row["name"]) == trim($name))
                return $row["strength"];
        }

        return $name.' not found';
    }

    public function compare(String $teamOne, String $teamTwo) : string
    {
        if ($this->getStrength($teamOne) > $this->getStrength($teamTwo))
            return $teamOne;

        return $teamTwo;
    }
}
