<?php

namespace App\Models;

class Predictions
{
    protected $rules;

    public function __construct(Rules $rules)
    {
        $this->rules = $rules;
    }

    public function getByStandings(array $standings) : array
    {
        usort($standings, function ($a, $b) {
            if ($a['pts'] === $b['pts'])
                return 0;

            return $a['pts'] < $b['pts'] ? 1 : -1;
        });

        $maxPTS = max(array_column($standings, 'pts'));

        $duplicatesOnPTS = $this->searchForDuplicates("pts", $standings, $maxPTS);

        if (count($duplicatesOnPTS) > 1) {

            $maxGD  = max(array_column($duplicatesOnPTS, 'gd'));

            $duplicatesOnGD = $this->searchForDuplicates("gd", $duplicatesOnPTS, $maxGD);
            if (count($duplicatesOnGD) > 1) {

                $maxGoals  = max(array_column($duplicatesOnGD, 'goals'));
                $duplicatesOnGoals = $this->searchForDuplicates("goals", $duplicatesOnGD, $maxGoals);
                $winnerPoints = $duplicatesOnGoals[0]['pts'] + 1;
                $standings = array_map(function ($standing) use ($duplicatesOnGoals) {
                    if ($standing["name"] === $duplicatesOnGoals[0]["name"])
                        $standing["pts"] += 1;

                    return $standing;
                }, $standings);
            } else {
                $winnerPoints = $duplicatesOnGD[0]['pts'] + 1;
                $standings = array_map(function ($standing) use ($duplicatesOnGD) {
                    if ($standing["name"] === $duplicatesOnGD[0]["name"])
                        $standing["pts"] += 1;

                    return $standing;
                }, $standings);
            }
        } else {
            $winnerPoints = $duplicatesOnPTS[0]['pts'] + 1;
            $standings = array_map(function ($standing) use ($duplicatesOnPTS) {
                if ($standing["name"] === $duplicatesOnPTS[0]["name"]) {
                    $standing["pts"] += 1;
                }
                return $standing;
            }, $standings);
        }

        $predictions = $this->getPredictionPercentages($winnerPoints, $standings);

        return $predictions;
    }

    public function getPredictionPercentages(int $winnerPoints, array $standings) : array
    {
        $predictions = [];

        foreach ($standings as $player) {
            $predictions[$player["name"]] = ($player['pts'] * 100) / $winnerPoints;
        }

        $finetuneConstant = array_sum($predictions);

        foreach ($predictions as $key => $value) {
            $predictions[$key] = ($value / $finetuneConstant) * 100;
        }

        return $predictions;
    }

    function searchForDuplicates(string $record, array $standings, string $value) : array
    {
        $duplicates = [];
        foreach ($standings as $standing) {
            if ($standing[$record] === $value)
                array_push($duplicates, $standing);
        }
        return $duplicates;
    }
}
