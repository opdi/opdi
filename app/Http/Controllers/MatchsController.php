<?php

namespace App\Http\Controllers;

use App\Models\Matchs;

class MatchsController
{
    protected $matchs;

    public function __construct(Matchs $matchs)
    {
        $this->matchs = $matchs;
    }

    public function get() : array
    {
        return $this->matchs->getByWeeks();
    }
}
