<?php

namespace App\Http\Controllers;

use App\Models\Predictions;
use Illuminate\Http\Request;

class PredictionsController
{
    protected $predictions;

    public function __construct(Predictions $predictions)
    {
        $this->predictions = $predictions;
    }

    public function get(Request $request) : array
    {
        $standings = $request->standings;
        return $this->predictions->getByStandings($standings);
    }
}
