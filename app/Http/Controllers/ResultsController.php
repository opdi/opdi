<?php

namespace App\Http\Controllers;

use App\Models\Results;
use Illuminate\Http\Request;

class ResultsController
{
    protected $results;

    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    public function get(Request $request) : array
    {
        $matchs = $request->matchs;
        return $this->results->getByWeeks($matchs);
    }
}
