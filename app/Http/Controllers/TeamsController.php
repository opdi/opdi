<?php

namespace App\Http\Controllers;

use App\Models\Teams;
use Illuminate\Routing\Controller as BaseController;

class TeamsController extends BaseController
{
    protected $teams;

    public function __construct(Teams $teams)
    {
        $this->teams = $teams;
    }

    public function get() : object
    {
        return $this->teams->get(['name','image','strength']);
    }
}
