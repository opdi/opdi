$(function() {
    switch(window.location.pathname) {
        case "/":
            generateTeamList();
            break;
        case "/simulation":
            generateLeague();
            getMatchsByWeek();
            break;
        default:
            console.log("page not found");
    }
});

window.week = 0;
window.automatic = false;
var teams = "";

function startSimulation() {
    $("#container_fixtures").hide();
    $("#simulation").show();
}

function playAllWeeks() {
    window.automatic = true;
    getNextWeekResults()
}

function SortByPTS(a, b){
    var aPTS = a.pts;
    var bPTS = b.pts;
    var aGD = a.gd;
    var bGD = b.gd;

    return ((aPTS > bPTS) ? -1 : ((aPTS < bPTS) ? 1 : (((aGD > bGD) ? -1 : ((aGD < bGD) ? 1 : 0)))));
}

function generateTeamList() {
    if(!teams)
        getTeams("generateTeamList");

    $('#teams > tbody').html(`
                            <tr>
                                <th scope="row">Team</th>
                                <th scope="row">Strength</th>
                            </tr>
                        `);

    $.each(teams, function(key, team) {
        $('#teams > tbody').append(`
                            <tr id="team-${key}">
                                <td scope="row">
                                    <img src="${team.image}" />
                                    ${team.name}
                                </td>
                                <td scope="row">${team.strength}</td>
                            </tr>
                        `);
    });
}

function generateLeague() {
    if(!teams)
        getTeams("generateLeague");

    window.leagueTable = [];
    $.each(teams, function(key, team) {
        leagueTable[key] = {
            name: team.name,
            image: team.image,
            pts : 0,
            p   : 0,
            w   : 0,
            d   : 0,
            l   : 0,
            gd  : 0,
            goals: 0
        };
        team = leagueTable[key]
        $('#league > tbody').append(`
                            <tr id="team-${key}">
                                <td scope="row" class="${team.name}">
                                    <img src="${team.image}" />
                                    ${team.name}
                               </td>
                                <td id="pts-${key}">${team.pts}</td>
                                <td id="p-${key}">${team.p}</td>
                                <td id="w-${key}">${team.w}</td>
                                <td id="d-${key}">${team.d}</td>
                                <td id="l-${key}">${team.l}</td>
                                <td id="gd-${key}">${team.gd}</td>
                            </tr>
                        `);
    });
}

function refreshLeagueTable() {
    window.leagueTable.sort(SortByPTS);

    $('#league > tbody').html('');
    $.each(window.leagueTable, function(key, team) {
        $('#league > tbody').append(`
                    <tr id="team-${key}">
                        <td scope="row" class="${team.name}">
                            <img src="${team.image}" />
                            ${team.name}
                        </td>
                        <td id="pts-${key}">${team.pts}</td>
                        <td id="p-${key}">${team.p}</td>
                        <td id="w-${key}">${team.w}</td>
                        <td id="d-${key}">${team.d}</td>
                        <td id="l-${key}">${team.l}</td>
                        <td id="gd-${key}">${team.gd}</td>
                    </tr>
                `);
    });
}

function setLeagueTable(playerOne,scorePlayerOne, playerTwo, scorePlayerTwo) {
    window.leagueTable.map((teamRecords, index) => {
        if (teamRecords.name === playerOne) {
            window.leagueTable[index].p++;
            window.leagueTable[index].goals++;
            if (scorePlayerOne > scorePlayerTwo) {
                window.leagueTable[index].w++;
                window.leagueTable[index].pts = window.leagueTable[index].pts + 3;
            }
            if (scorePlayerOne < scorePlayerTwo) {
                window.leagueTable[index].l++;
            }
            if (scorePlayerOne === scorePlayerTwo) {
                window.leagueTable[index].d++;
                window.leagueTable[index].pts = window.leagueTable[index].pts + 1;
            }
            window.leagueTable[index].gd = window.leagueTable[index].gd + scorePlayerOne - scorePlayerTwo;
        }

        if (teamRecords.name === playerTwo) {
            window.leagueTable[index].p++;
            window.leagueTable[index].goals++;
            if (scorePlayerOne < scorePlayerTwo) {
                window.leagueTable[index].w++;
                window.leagueTable[index].pts = window.leagueTable[index].pts + 3;
            }
            if (scorePlayerOne > scorePlayerTwo) {
                window.leagueTable[index].l++;
            }
            if (scorePlayerOne === scorePlayerTwo) {
                window.leagueTable[index].d++;
                window.leagueTable[index].pts = window.leagueTable[index].pts + 1;
            }
            window.leagueTable[index].gd = window.leagueTable[index].gd + scorePlayerTwo - scorePlayerOne;
        }
    });

    refreshLeagueTable();
}

function setWeekResults(playerOne,scorePlayerOne, playerTwo, scorePlayerTwo) {
    $('#week_results_holder').prepend(
        `
                    <div class="match-result">
                    <div class="row">
                        <div class="col-md-5 ${playerOne}">
                                ${playerOne}
                            </div>
                            <div class="col-md-2">
                                ${scorePlayerOne} - ${scorePlayerTwo}
                            </div>
                            <div class="col-md-5 ${playerTwo}">
                                ${playerTwo}
                            </div>
                        </div>
                    </div>
                `
    );
}

function getPredictions() {
    $.ajax({
        url : `predictions`,
        type : 'post',
        dataType : 'json',
        data: {
            "_token": token,
            standings : window.leagueTable
        },

        success : function(response){
            $('#predictions > tbody').html('');
            $.each(response, function(team, percentage) {
                percentage = parseInt(percentage);
                $('#predictions > tbody').append(`
                            <tr>
                                <td class="${team}">${team}</td>
                                <td scope="row">${percentage}%</td>
                            </tr>`
                );
            });
        },
        error : function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            alert('Error - ' + errorMessage);
        }
    });
}

function getTeams(callback) {
    $.ajax({
        url : 'teams',
        type : 'GET',
        dataType : 'json',
        success : function(response){
            teams = response;
            eval(callback+"()");
        },
        error : function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            alert('Error - ' + errorMessage);
        }
    });
}

function generateFixtures() {
    $.ajax({
        url : 'matchs',
        type : 'GET',
        dataType : 'json',
        success : function(response){
            $("#generate_fixtures_btn").addClass("disabled");
            $("#container_fixtures").show();
            $.each(response, function(key, value) {
                $('#fixtures').append(
                    `
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                Week : ${key}
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5">
                                        ${value[0][0]}
                                    </div>
                                    <div class="col-md-2">
                                        -
                                    </div>
                                    <div class="col-md-5">
                                        ${value[0][1]}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        ${value[1][0]}
                                    </div>
                                    <div class="col-md-2">
                                        -
                                    </div>
                                    <div class="col-md-5">
                                        ${value[1][1]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `
                );
            });
        },
        error : function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            alert('Error - ' + errorMessage);
        }
    });
}

function getMatchsByWeek() {
    $.ajax({
        url : 'matchs',
        type : 'GET',
        dataType : 'json',
        success : function(response){
            window.weeks = response;
        },
        error : function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            alert('Error - ' + errorMessage);
        }
    });
}

function getNextWeekResults() {
    if (window.week < Object.keys(window.weeks).length) {
        $(".btn-danger").removeClass("disabled");

        window.week++;

        $.ajax({
            url : `results`,
            type : 'post',
            dataType : 'json',
            data: {
                "_token": token,
                matchs : window.weeks[window.week]
            },

            success : function(response){
                $.each(response, function(key, match) {
                    let playerOne = Object.keys(match)[0];
                    let scorePlayerOne = match[playerOne];

                    let playerTwo = Object.keys(match)[1];
                    let scorePlayerTwo = match[playerTwo];

                    setLeagueTable(playerOne,scorePlayerOne, playerTwo, scorePlayerTwo);

                    setWeekResults(playerOne,scorePlayerOne, playerTwo, scorePlayerTwo);
                });

                $('#week_results_holder').prepend(
                    `
                            <hr>
                            <h5 id="week-number">
                                Week : <span>`+window.week+`</span>
                            </h5>
                        `
                );

                if (window.week > 3)
                    getPredictions()

                if (window.automatic)
                    getNextWeekResults()

                if (window.week == Object.keys(window.weeks).length) {
                    //$("#team-0").css("background-color","dimgray");
                    $("#team-0").css("background-color","#e74c3c");
                    $(".btn-info").addClass("disabled");
                }
            },
            error : function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });
    }
}
