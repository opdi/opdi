<?php

namespace Tests\Feature;

use Tests\TestCase;

class PagesTest extends TestCase
{
    public function test_home()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_simulation_page()
    {
        $response = $this->get('/simulation');

        $response->assertStatus(200);
    }

    public function test_teams_ajax()
    {
        $response = $this->get('/teams');

        $response->assertStatus(200);
    }

    public function test_matchs_ajax()
    {
        $response = $this->get('/matchs');

        $response->assertStatus(200);
    }
}
