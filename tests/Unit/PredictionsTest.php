<?php

namespace Tests\Unit;

use App\Models\Predictions;
use Tests\TestCase;
use App\Models\Rules;

class PredictionsTest extends TestCase
{
    public function test_get_matchs()
    {
        $rules = new Rules();

        $m_predictions = new Predictions($rules);
        $result = $m_predictions->getByStandings($this->getSampleData());

        // 4 takım için 4 tahmin
        $this->assertEquals(4, count($result));
    }

    public function test_get_prediction_percentages()
    {
        $rules = new Rules();

        $m_predictions = new Predictions($rules);
        $result = $m_predictions->getPredictionPercentages(4, $this->getSampleData());

        // 4 takım için 4 tahmin
        $this->assertEquals(4, count($result));
        // ihtimaller toplamı 100 olmalı
        $this->assertEquals(100, array_sum($result));
    }

    public function test_search_for_duplicates()
    {
        $rules = new Rules();

        $m_predictions = new Predictions($rules);
        $result = $m_predictions->searchForDuplicates('pts', $this->getSampleData(), 2);

        $this->assertEmpty($result);
    }

    public function getSampleData()
    {
        return array (
            0 =>
                array (
                    'name' => 'Liverpool',
                    'pts' => '3',
                    'p' => '1',
                    'w' => '1',
                    'd' => '0',
                    'l' => '0',
                    'gd' => '0',
                    'goals' => '1',
                ),
            1 =>
                array (
                    'name' => 'Manchester City',
                    'pts' => '0',
                    'p' => '1',
                    'w' => '0',
                    'd' => '0',
                    'l' => '1',
                    'gd' => '-1',
                    'goals' => '1',
                ),
            2 =>
                array (
                    'name' => 'Arsenal',
                    'pts' => '3',
                    'p' => '1',
                    'w' => '1',
                    'd' => '0',
                    'l' => '0',
                    'gd' => '0',
                    'goals' => '1',
                ),
            3 =>
                array (
                    'name' => 'Chelsea',
                    'pts' => '0',
                    'p' => '1',
                    'w' => '0',
                    'd' => '0',
                    'l' => '1',
                    'gd' => '-1',
                    'goals' => '1',
                ),
        );
    }

}
