<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Teams;

class TeamsTest extends TestCase
{
    public function test_get_team_names()
    {
        $m_team = new Teams;
        $result = $m_team->getNames();

        $this->assertIsArray($result);
    }

    public function test_get_all_teams_for_already_set()
    {
        $m_team = new Teams;
        $m_team->team_list = (object)['GS'];
        $result = $m_team->get();

        $this->assertEquals((object)['GS'],$result);
    }

    public function test_get_team_strength()
    {
        $m_team = new Teams;
        $result = $m_team->getStrength('Chelsea');

        $this->assertNotEquals('Chelsea not found', $result);
    }

    public function test_compare_team()
    {
        $m_team = new Teams;
        $result = $m_team->compare('Chelsea', 'Liverpool');

        $this->assertTrue(in_array($result, ['Chelsea', 'Liverpool']));
    }
}
