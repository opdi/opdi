<?php

namespace Tests\Unit;

use App\Models\Matchs;
use App\Models\Teams;
use App\Models\Rules;
use Tests\TestCase;
use App\Models\Results;

class ResultsTest extends TestCase
{
    public function test_get_match_result()
    {
        $teams = new Teams();
        $rules = new Rules();
        $matchs = new Matchs($teams, $rules);

        $m_resuls = new Results($teams, $matchs);
        $result = $m_resuls->getByMatches('Chelsea', 'Liverpool');

        $this->assertIsArray($result);
        $this->assertTrue(($result['Chelsea'] <= 5 && $result['Chelsea'] >= 0));
        $this->assertTrue(($result['Liverpool'] <= 5 && $result['Liverpool'] >= 0));
    }

    public function test_get_week_results()
    {
        $teams = new Teams();
        $rules = new Rules();
        $matchs = new Matchs($teams, $rules);

        $m_resuls = new Results($teams, $matchs);

        $result = $m_resuls->getByWeeks([
            ["Liverpool","Manchester City"],
            ["Arsenal","Chelsea"]
        ]);

        $this->assertIsArray($result);
        // Sadece array mi kontrolü yapmak yeterli, diğer durumlar test_get_match_result içinde kontrol edildi zaten
    }
}
