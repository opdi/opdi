<?php

namespace Tests\Unit;

use App\Models\Matchs;
use Tests\TestCase;
use App\Models\Teams;
use App\Models\Rules;

class MatchsTest extends TestCase
{
    public function test_get_matchs()
    {
        $teams = new Teams();
        $rules = new Rules();

        $m_matchs = new Matchs($teams, $rules);
        $result = $m_matchs->getByWeeks();

        // 4 takım için 6 hafta oluşmalı
        $this->assertEquals(6, count($result));
        // 4 takım için her hafta 2 maç olmalı
        $this->assertEquals(2, count($result[1]));
        // her maç 2 takımla yapılır
        $this->assertEquals(2, count($result[1][0]));
    }
}
